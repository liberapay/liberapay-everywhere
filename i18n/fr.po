msgid ""
msgstr ""
"Project-Id-Version: Liberapay Everywhere 0.1\n"
"POT-Creation-Date: 2018-04-14 17:11+0100\n"
"PO-Revision-Date: 2019-11-01 23:03+0000\n"
"Last-Translator: Éfrit <Efrit@posteo.net>\n"
"Language-Team: French <https://hosted.weblate.org/projects/liberapay/liberapay-everywhere/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.10-dev\n"

# Firefox summary
msgid "The official Liberapay addon for your browser, we enable Liberapay tipping icons on many of your favorite sites. It currently supports Bitbucket, GitHub and Twitter."
msgstr "L’extension officielle de Liberapay pour votre navigateur. Elle ajoute un bouton de don récurrent Liberapay sur plusieurs de vos sites préférés. Elle prend actuellement en charge Bitbucket, GitHub et Twitter."

# Firefox tags
msgid "donations, gratipay, liberapay, money, patreon, social, support"
msgstr "dons, gratipay, liberapay, argent, patreon, social, soutien"

# Firefox screenshot 1
msgid "The extension page action appears on valid websites; it can be clicked"
msgstr "Le bouton de don récurrent apparaît sur les sites Web valides."

# Firefox screenshot 2
msgid "When clicked, it redirects us to the Liberapay account corresponding to that website, if there is any."
msgstr "En cliquant dessus, vous êtes redirigé vers le compte Liberapay correspondant à ce site, s’il existe."

# Firefox screenshot 3
msgid "If there isn't any corresponding Liberapay profile, we can make a pledge."
msgstr "S’il n’y a de profil Liberapay correspondant, vous pouvez faire une promesse de don."
